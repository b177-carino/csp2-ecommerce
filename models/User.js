const mongoose = require("mongoose"); 

const userSchema = new mongoose.Schema({
    firstName : {
        type: String,
        required: [true, "First name is required"]
    },

    lastName : {
        type: String,
        required: [true, "Last name is required"]
    },

    phoneNumber : {
        type: Number,
        required: [true, "Phone number is required"]
    },
    
    email: {
        type: String, 
        required: [true, "Email is required"]
    },
    password: {
        type: String, 
        required: [true, "Password is required"]
    },
    isAdmin: {
        type: Boolean,
        default: false
    },
    order: [{
        purchasedOn: {
            type: Date,
            default: new Date()
        },
        productId: {
            type: String,
            required: [true, "Product Id is required"]
        }
    }]
})

module.exports = mongoose.model("User", userSchema)