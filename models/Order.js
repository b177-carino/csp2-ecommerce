const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        require: [true, "User Id is required"]
    },
    productId : {
        type: String,
        required: [true, "Product Id is required"]
    },
    purchasedOn : {
        type: Date,
        default: new Date()
    },
    quantity: {
        type: Number,
        
    },
    totalAmount: {
        type: Number,
        
    }
})

module.exports = mongoose.model("Order", orderSchema)