const express = require("express");
const router = express.Router()
const auth = require("../auth")
const productController = require("../controllers/product")

// Create Product Sample Workflow
// 1. An authenticated admin user sends a POST request containing a JWT in its header to the /products endpoint.
// 2. API validates JWT, returns false if validation fails.
// 3. If validation successful, API creates product using the contents of the request body.

router.post("/", auth.verify, (req, res) => {
    const data = {
        product : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.addProduct(data).then(resultFromController => res.send(resultFromController));
})



// Retrieve All Active Products Sample Workflow
// 1. A GET request is sent to the /products endpoint.
// 2. API retrieves all active products and returns them in its response.

router.get("/", (req, res) => { 
    productController.getActiveProduct().then(resultFromController => res.send(resultFromController));
})

router.get("/all", auth.verify, (req, res) => { 
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.getAllProduct(data).then(resultFromController => res.send(resultFromController));
})

// Retrieve Single Product Sample Workflow
// 1. A GET request is sent to the /products/:productId endpoint.
// 2. API retrieves product that matches productId URL parameter and returns it in its response.
router.get("/:productId", (req, res) => {
    productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Update Product Sample Workflow
// 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId endpoint.
// 2. API validates JWT, returns false if validation fails.
// 3. If validation successful, API finds product with ID matching the productId URL parameter and overwrites its info with those from the request body.

router.put("/:productId", auth.verify, (req,res) => {
    const data = {
        product : req.body,
        productId : req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.updateProduct(data).then(resultFromController => res.send (resultFromController));
})

// Archive Product Sample Workflow
// 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /products/:productId/archive endpoint.
// 2. API validates JWT, returns false if validation fails.
// 3. If validation successful, API finds product with ID matching the productId URL parameter and sets its isActive property to false.
router.put("/:productId/archive", auth.verify, (req, res) => {
    const archivedProduct = {
        productId: req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.archiveProduct(archivedProduct).then(resultFromController => res.send(resultFromController));
})

router.put("/:productId/activate", auth.verify, (req, res) => {
    const activateProduct = {
        productId: req.params.productId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }
    productController.activateProduct(activateProduct).then(resultFromController => res.send(resultFromController));
})


// router.delete("/deleteProduct", (req, res)=> {
//     const data = {
//         productId : req.body.productId,
//     }
//     productController.deleteProduct(data).then(resultFromController => res.send(resultFromController));
// })

module.exports = router;    