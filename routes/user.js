const express = require("express");
const router = express.Router();

const auth = require("../auth")

// Import User controller
const userController = require("../controllers/user")

// User Registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})


router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {

    const data = {
        userId : req.params.userId,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

	userController.setAdmin(data).then(resultFromController => res.send(resultFromController));
})

//Create an order
router.post("/checkout", (req, res) => {
    const data = {
        userId : auth.decode(req.headers.authorization).id,
        productId: req.body.productId,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userController.newOrder(data).then(resultFromController => res.send(resultFromController));
})



//Retrieve All Orders Sample Workflow
router.get("/orders", auth.verify, (req, res) => {
    const data = {
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    userController.getOrders(data).then(resultFromController => res.send(resultFromController));
})

// Retrieve Authenticated User’s Orders Sample Workflow
router.get("/myOrders", auth.verify, (req, res) => {
    const data = {
        isAdmin : auth.decode(req.headers.authorization).isAdmin,
        userId : auth.decode(req.headers.authorization).id
    }
    userController.myOrders(data).then(resultFromController => res.send(resultFromController));
})

router.get("/details", auth.verify, (req, res) => {

	// Uses the "decode" method defined in the "auth.js" file to retrieve the user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

module.exports = router;