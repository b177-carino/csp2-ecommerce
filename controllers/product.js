const Product = require("../models/Product")

const auth = require("../auth");
const { trusted } = require("mongoose");
const User = require("../models/User");

// Create Product Sample Workflow
module.exports.addProduct = (data) => {
    if(data.isAdmin){
        let newProduct = new Product ({
            name : data.product.name,
            description : data.product.description,
            price : data.product.price
        });

        return newProduct.save().then((product,error) => {
            if(error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// Retrieve All Products Sample Workflow
module.exports.getActiveProduct = () => {
    return Product.find({isActive : true }).then(result => {
        return result; 
    })
}

module.exports.getAllProduct = (data) => {
    if (data.isAdmin){
    return Product.find({}).then(result => {
        return result;    
        })
    }   
}

// Retrieve Single Product Sample Workflow
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}

// Update Product Sample Workflow
module.exports.updateProduct = (data) => {
    if (data.isAdmin){
        let updatedProduct =  {
            name : data.product.name,
            description : data.product.description,
            price : data.product.price,
        };
        
        return Product.findByIdAndUpdate(data.productId, updatedProduct).then((product, error) => {
            if(error) {
                return false;
            }
            else{
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false);
    }
}

// Archive Product Sample Workflow
module.exports.archiveProduct = (archivedProduct) => {
    if(archivedProduct.isAdmin){
        let newArchiveProduct = {
            isActive: false
        };
        
        return Product.findByIdAndUpdate(archivedProduct.productId, newArchiveProduct).then((product, error) => {
            if(error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false)
    }
}


module.exports.activateProduct = (activateProduct) => {
    if(activateProduct.isAdmin){
        let newActivateProduct = {
            isActive: true
        };
        
        return Product.findByIdAndUpdate(activateProduct.productId, newActivateProduct).then((product , error) => {
            if(error) {
                return false;
            }
            else {
                return true;
            }
        })
    }
    else {
        return Promise.resolve(false)
    }
}


// module.exports.deleteProduct = (data) => {
//     return Product.findByIdAndRemove(data.productId).then((result, error)=> {
//         if(error){
//             return false;
//         }
//         else{
//             return result;
//         }
//     })
// }
