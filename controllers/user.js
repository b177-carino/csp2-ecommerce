// Import User model
const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")

const bcrypt = require("bcrypt")
// Import auth
const auth = require("../auth")

// Controller function for user registration

module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({email : reqBody.email}).then(result => {

		// The "find" method returns a record if a match is found
		if (result.length > 0) {

			return true;

		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;

		};
	});

};



module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        phoneNumber : reqBody.phoneNumber,
        password : bcrypt.hashSync(reqBody.password,10)
    })


    return newUser.save().then((user, error) => {
        if(error) {
            return false;
        }
        else{
            return true;
        }
    })
}

// Controller function for user authentication
module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(result => {
        // User doesn't exist
        if(result === null) {
            return false
        }
        else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect) {
                // Generate access token
                return {access : auth.createAccessToken(result)}
            }
            // Password do not atch
            else {
                return false;
            }
        }
    })
}

// Set User as Admin workflow:
// 1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
// 2. API validates JWT, returns false if validation fails.
// 3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.
// module.exports.setAdmin = (data) => {
    
module.exports.setAdmin = (data) => {

    if (data.isAdmin) {
        let updateAdminField = {
            isAdmin : true
        };

        return User.findByIdAndUpdate(data.userId, updateAdminField).then((user, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
		return Promise.resolve(false);
	}

}

//Create an order
module.exports.newOrder = async (data) => {
    if(!(data.isAdmin)) {
        let isOrderUpdated = await Product.findById(data.productId).then(product => {
            if(product.isActive == true) {
                let newOrder = new Order ({
                    userId : data.userId,
                    productId : data.productId,
                    purchaseOn: new Date(),
                })
                return newOrder.save().then((product, error) => {
                    if(error){
                        return false;
                    }
                    else {
                        return true; 
                    }
                })
            }
            else {
                return false;
            }
    })
        let isUserUpdated = await User.findById(data.userId).then(user => {
            user.order.push({productId : data.productId})

            return user.save().then((user, error) =>{
                if(error) {
                    return false;
                }
                else {
                    return true;
                }
            })
        })
        let isProductUpdated = await Product.findById(data.productId).then(product => {
            product.order.push({userId : data.userId})

            return product.save().then((product, error) => {
                if(error){
                    return false;
                }
                else {
                    return true;
                }
            })
        })
        if(isUserUpdated && isProductUpdated && isOrderUpdated){
            return "Order has been created";
        }
        else {
            return "Order can't be created" ;
        }
    }
    else {
        return Promise.resolve("Can't process order, check permission")
    }
}

// Retrieve All Orders Sample Workflow
module.exports.getOrders = (data) => {
    if(data.isAdmin){
        return Order.find({}).then(result => {
            return result
        })
    }
    else {
        return Promise.resolve("Permission denied")
    }
}


// Retrieve Authenticated User’s Orders Sample Workflow
module.exports.myOrders = (data) => {
    return Order.find({userId : data.userId}).then(order => {
        if(!data.isAdmin) {
            return order;
        }
        else {
            return "Permission denied"
        }
    })
}

module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;

	});

};