const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// Allows access to routes
const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

mongoose.connect("mongodb+srv://admin:admin@wdc028-course-booking.papx5.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
});
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas"));

// CORS
app.use(cors());

// Express
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// User route
app.use("/users", userRoutes);
app.use("/products", productRoutes);


app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`)
});